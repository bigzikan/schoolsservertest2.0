﻿using schoolsservertest2._0.Data.Interfaces;
using schoolsservertest2._0.Data.Models.DTO;

namespace schoolsservertest2._0.Services
{
    public class StudentsMigrationsService
    {
        private readonly IStudentsMigrations _studentsMigrations;

        public StudentsMigrationsService(IStudentsMigrations studentsMigrations)
        {
            _studentsMigrations = studentsMigrations;
        }

        public async Task<bool> AddStudentInСlass(StudentMigration model)
        {
            return await _studentsMigrations.AddStudentInСlass(model);
        }

        public async Task<bool> RemoveStudentFromСlass(StudentMigration removeModel)
        {
            return await _studentsMigrations.RemoveStudentFromСlass(removeModel);
        }

        public async Task<List<StudentMigration>> GetMigrationStudentHistory()
        {
            return await _studentsMigrations.GetMigrationStudentHistory();
        }       

        public async Task<List<StudentMigration>> GetMigrationStudentHistoryByStudentId(int studentId)
        {
            return await _studentsMigrations.GetMigrationStudentHistoryByStudentId(studentId);
        }

    }
}
