﻿using schoolsservertest2._0.Data.Interfaces;
using schoolsservertest2._0.Data.Models.DTO;

namespace schoolsservertest2._0.Services
{
    public class StudentsService
    {
        private readonly IStudent _students;

        public StudentsService(IStudent students)
        {
            _students = students;
        }

        public async Task<List<Student>> GetAllStudents()
        {
            return await _students.GetAllStudents();
        }

        public async Task<Student?> GetStudentById(int id)
        {
            return await _students.GetStudentById(id);
        }

        public async Task<int> CreateStudent(Student model)
        {
            return await _students.CreateStudent(model);
        }
    }
}
