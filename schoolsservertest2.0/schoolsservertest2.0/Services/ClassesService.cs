﻿using schoolsservertest2._0.Data.Interfaces;
using schoolsservertest2._0.Data.Models.DTO;

namespace schoolsservertest2._0.Services
{
    public class ClassesService
    {
        private readonly IClass _classes;

        public ClassesService (IClass classes)
        {
            _classes = classes;
        }

        public async Task<List<SchoolClass>> GetAllClasses()
        {
            return await _classes.GetAllClasses();
        }

        public async Task<SchoolClass?> GetClassById(int id)
        {
            return await _classes.GetClassById(id);
        }

        public async Task<int> CreateClass(SchoolClass model)
        {
            return await _classes.CreateClass(model);
        }

        public async Task<List<Student>> GetStudentsByClassId(int classId)
        {
            return await _classes.GetStudentsByClassId(classId);
        }
    }
}
