﻿using schoolsservertest2._0.Data.Interfaces;
using schoolsservertest2._0.Data.Models.DTO;

namespace schoolsservertest2._0.Services
{
    public class SchoolsService
    {
        private readonly ISchool _school;

        public SchoolsService(ISchool school)
        {
            _school = school;
        }

        public async Task<List<School>> GetAllSchools()
        {
            return await _school.GetAllSchools();
        }

        public async Task<School?> GetSchoolById(int id)
        {
            return await _school.GetSchoolById(id);
        }

        public async Task<int> CreateSchool(School model)
        {
            return await _school.CreateSchool(model);
        }

        public async Task<List<SchoolClass>> GetClassesBySchoolId(int schoolId)
        {
            return await _school.GetClassesBySchoolId(schoolId);
        }
    }
}
