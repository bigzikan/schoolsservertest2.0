﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using schoolsservertest2._0.Data.Interfaces;
using schoolsservertest2._0.Data.Models.DTO;
using schoolsservertest2._0.Services;
using System.Web.Http;
using FromBodyAttribute = Microsoft.AspNetCore.Mvc.FromBodyAttribute;
using HttpGetAttribute = Microsoft.AspNetCore.Mvc.HttpGetAttribute;
using HttpPostAttribute = Microsoft.AspNetCore.Mvc.HttpPostAttribute;
using RouteAttribute = Microsoft.AspNetCore.Mvc.RouteAttribute;

namespace schoolsservertest2._0.Controllers
{
    [RoutePrefix("api/[controller]")]
    [ApiController]
    public class SchoolsController : ControllerBase
    {
        private SchoolsService _schoolsService;

        public SchoolsController(ISchool school)
        {
            this._schoolsService = new SchoolsService(school);
        }

        /// <summary>
        /// Возвращает список школ
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getAllSchools")]
        public async Task<IActionResult> GetAllSchools()
        {
            List<School> schools = new List<School>();

            schools = await _schoolsService.GetAllSchools();

            return Ok(schools);
        }

        /// <summary>
        /// Возвращает информацию о школе по Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getSchoolById/{id}")]
        public async Task<IActionResult> GetSchoolById(int id)
        {
            School? school = new School();

            school = await _schoolsService.GetSchoolById(id);

            return Ok(school);
        }

        /// <summary>
        /// Добавляет инфо о школе
        /// </summary>
        /// <param name="model"></param>
        /// <returns>id созданной школы</returns>
        [HttpPost]
        [Route("createSchool")]
        public async Task<IActionResult> CreateSchool(
            [FromBody] School model
            )
        {
            try
            {
                var newSchoolId = await _schoolsService.CreateSchool(model);
                return Ok(newSchoolId);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }            
        }

        /// <summary>
        /// Возвращает информацию о классах по Id школы
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]        
        [Route("getClassesBySchoolId/{schoolId}")]
        public async Task<IActionResult> GetClassesBySchoolId(int schoolId)
        {
            var classesList = new List<SchoolClass>();

            classesList = await _schoolsService.GetClassesBySchoolId(schoolId);

            return Ok(classesList);
        }
    }
}
