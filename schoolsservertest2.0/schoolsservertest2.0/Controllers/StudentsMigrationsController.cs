﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using schoolsservertest2._0.Data.Interfaces;
using schoolsservertest2._0.Data.Models.DTO;
using schoolsservertest2._0.Services;
using System.Web.Http;
using FromBodyAttribute = Microsoft.AspNetCore.Mvc.FromBodyAttribute;
using HttpGetAttribute = Microsoft.AspNetCore.Mvc.HttpGetAttribute;
using HttpPostAttribute = Microsoft.AspNetCore.Mvc.HttpPostAttribute;
using RouteAttribute = Microsoft.AspNetCore.Mvc.RouteAttribute;

namespace schoolsservertest2._0.Controllers
{
    [RoutePrefix("api/[controller]")]
    [ApiController]
    public class StudentsMigrationsController : ControllerBase
    {
        private StudentsMigrationsService _studentsMigrationsService;

        public StudentsMigrationsController(IStudentsMigrations studentsMigrations)
        {
            _studentsMigrationsService = new StudentsMigrationsService(studentsMigrations);
        }

        /// <summary>
        /// Зачислить ученика в класс
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("addStudentInСlass")]
        public async Task<IActionResult> AddStudentInСlass([FromBody] StudentMigration model)
        {
            var response = await _studentsMigrationsService.AddStudentInСlass(model);
            return Ok(response);
        }

        /// <summary>
        /// Зачислить ученика в класс
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("removeStudentFromСlass")]
        public async Task<IActionResult> RemoveStudentFromСlass([FromBody] StudentMigration model)
        {
            var response = await _studentsMigrationsService.RemoveStudentFromСlass(model);
            return Ok(response);
        }

        /// <summary>
        /// Получить всю историю перемещений школьников
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("getMigrationStudentHistory")]
        public async Task<IActionResult> GetMigrationStudentHistory()
        {
            var response = await _studentsMigrationsService.GetMigrationStudentHistory();
            return Ok(response);
        }

        /// <summary>
        /// Получить всю историю перемещений школьника
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getMigrationStudentHistoryByStudentId/{studentId}")]
        public async Task<IActionResult> GetMigrationStudentHistoryByStudentId(int studentId)
        {
            var response = await _studentsMigrationsService.GetMigrationStudentHistoryByStudentId(studentId);
            return Ok(response);
        }
    }
}
