﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using schoolsservertest2._0.Data.Interfaces;
using schoolsservertest2._0.Data.Models.DTO;
using schoolsservertest2._0.Services;
using System.Web.Http;
using FromBodyAttribute = Microsoft.AspNetCore.Mvc.FromBodyAttribute;
using HttpGetAttribute = Microsoft.AspNetCore.Mvc.HttpGetAttribute;
using HttpPostAttribute = Microsoft.AspNetCore.Mvc.HttpPostAttribute;
using RouteAttribute = Microsoft.AspNetCore.Mvc.RouteAttribute;

namespace schoolsservertest2._0.Controllers
{
    [RoutePrefix("api/[controller]")]
    [ApiController]
    public class StudentsController : ControllerBase
    {
        private StudentsService _studentsService;

        public StudentsController(IStudent students)
        {
            this._studentsService = new StudentsService(students);
        }

        /// <summary>
        /// Возвращает список учеников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getAllStudents")]
        public async Task<IActionResult> GetAllStudents()
        {
            List<Student> schools = new List<Student>();

            schools = await _studentsService.GetAllStudents();

            return Ok(schools);
        }

        /// <summary>
        /// Возвращает информацию об ученике по Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getStudentById/{id}")]
        public async Task<IActionResult> GetStudentById(int id)
        {
            Student? student = new Student();

            student = await _studentsService.GetStudentById(id);

            return Ok(student);
        }

        /// <summary>
        /// Добавляет инфо об ученике
        /// </summary>
        /// <param name="model"></param>
        /// <returns>id созданной школы</returns>
        [HttpPost]
        [Route("createStudent")]
        public async Task<IActionResult> CreateStudent(
            [FromBody] Student model
            )
        {
            try
            {
                var newStudentId = await _studentsService.CreateStudent(model);
                return Ok(newStudentId);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
