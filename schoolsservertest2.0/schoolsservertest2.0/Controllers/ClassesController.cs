﻿using Azure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using schoolsservertest2._0.Data.Interfaces;
using schoolsservertest2._0.Data.Models.DTO;
using schoolsservertest2._0.Services;
using System.Web.Http;
using FromBodyAttribute = Microsoft.AspNetCore.Mvc.FromBodyAttribute;
using HttpGetAttribute = Microsoft.AspNetCore.Mvc.HttpGetAttribute;
using HttpPostAttribute = Microsoft.AspNetCore.Mvc.HttpPostAttribute;
using RouteAttribute = Microsoft.AspNetCore.Mvc.RouteAttribute;

namespace schoolsservertest2._0.Controllers
{
    [RoutePrefix("api/[controller]")]
    [ApiController]
    public class ClassesController : ControllerBase
    {
        private ClassesService _classesService;

        public ClassesController(IClass schoolClass)
        {
            this._classesService = new ClassesService(schoolClass);
        }

        /// <summary>
        /// Возвращает список классов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getAllClasses")]
        public async Task<IActionResult> GetAllClasses()
        {
            List<SchoolClass> classes = new List<SchoolClass>();

            classes = await _classesService.GetAllClasses();

            return Ok(classes);
        }

        /// <summary>
        /// Возвращает информацию о классе по Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getClassById/{id}")]
        public async Task<IActionResult> GetClassById(int id)
        {
            SchoolClass? schoolCLass = new SchoolClass();

            schoolCLass = await _classesService.GetClassById(id);

            return Ok(schoolCLass);
        }

        /// <summary>
        /// Добавляет инфо о классе
        /// </summary>
        /// <param name="model"></param>
        /// <returns>id созданного класса</returns>
        [HttpPost]
        [Route("createClass/getClassById/{id}")]
        public async Task<IActionResult> CreateClass(
            [FromBody] SchoolClass model
            )
        {
            try
            {
                var newSchoolId = await _classesService.CreateClass(model);
                return Ok(newSchoolId);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Возвращает информацию о классе по Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getStudentsByClassId/{classId}")]
        public async Task<IActionResult> GetStudentsByClassId(int classId)
        {
            var response = new List<Student>();

            response = await _classesService.GetStudentsByClassId(classId);

            return Ok(response);
        }
    }
}
