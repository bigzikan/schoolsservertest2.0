﻿using schoolsservertest2._0.Data.Models.DTO;

namespace schoolsservertest2._0.Data.Interfaces
{
    public interface IStudent
    {
        Task<List<Student>> GetAllStudents();
        Task<Student?> GetStudentById(int id);
        Task<int> CreateStudent(Student model);
    }
}
