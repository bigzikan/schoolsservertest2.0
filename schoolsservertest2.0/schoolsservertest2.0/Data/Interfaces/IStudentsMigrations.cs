﻿using schoolsservertest2._0.Data.Models.DTO;

namespace schoolsservertest2._0.Data.Interfaces
{
    public interface IStudentsMigrations
    {
        Task<List<StudentMigration>> GetMigrationStudentHistory();
        Task<bool> AddStudentInСlass(StudentMigration model);
        Task<bool> RemoveStudentFromСlass(StudentMigration removeModel);
        Task<List<StudentMigration>> GetMigrationStudentHistoryByStudentId(int studentId);
    }
}
