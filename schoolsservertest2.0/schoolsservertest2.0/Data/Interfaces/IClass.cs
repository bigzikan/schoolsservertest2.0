﻿using schoolsservertest2._0.Data.Models.DTO;

namespace schoolsservertest2._0.Data.Interfaces
{
    public interface IClass
    {
        Task<List<SchoolClass>> GetAllClasses();
        Task<SchoolClass?> GetClassById(int id);
        Task<int> CreateClass(SchoolClass model);
        Task<List<Student>> GetStudentsByClassId(int classId);
    }
}
