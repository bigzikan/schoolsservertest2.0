﻿using schoolsservertest2._0.Data.Models.DTO;

namespace schoolsservertest2._0.Data.Interfaces
{
    public interface ISchool
    {
        Task<List<School>> GetAllSchools();
        Task<School?> GetSchoolById(int id);
        Task<int> CreateSchool(School model);
        Task<List<SchoolClass>> GetClassesBySchoolId(int schoolId);
    }
}
