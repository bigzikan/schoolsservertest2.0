﻿namespace schoolsservertest2._0.Data.Models.DTO
{
    public class Student
    {
        /// <summary>
        /// Id ученика
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Имя ученика
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Фамилия ученика
        /// </summary>
        public string SecondName { get; set; }

        /// <summary>
        /// Отчество ученика
        /// </summary>
        public string? ThirdName { get; set; }

        /// <summary>
        /// Дата рождения ученика
        /// </summary>
        public DateTime BirthDate { get; set; }

        /// <summary>
        /// Адрес проживания ученика
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Id класса
        /// </summary>
        public int ClassesId { get; set; }
    }
}
