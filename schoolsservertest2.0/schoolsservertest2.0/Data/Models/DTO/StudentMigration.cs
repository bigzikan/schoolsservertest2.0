﻿namespace schoolsservertest2._0.Data.Models.DTO
{
    public class StudentMigration
    {
        /// <summary>
        /// Id записи о зачислении/отчислении
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Id школьника
        /// </summary>
        public int StudentId { get; set; }

        /// <summary>
        /// Id класса
        /// </summary>
        public int ClassId { get; set; }

        /// <summary>
        /// Дата зачисления
        /// </summary>
        public DateTime? EnrollmentDate { get; set; }

        /// <summary>
        /// Дата отчисления из класса
        /// </summary>
        public DateTime? DeductionDate { get; set; }
    }
}
