﻿namespace schoolsservertest2._0.Data.Models.DTO
{
    public class SchoolClass
    {
        /// <summary>
        /// Id класса
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Номер класса
        /// </summary>
        public int ClassNumber { get; set; }

        /// <summary>
        /// Литерал класса
        /// </summary>
        public string? ClassLiteral { get; set; }

        /// <summary>
        /// Id школы
        /// </summary>
        public int SchoolId { get; set; }

        /// <summary>
        /// Описание/уклон класса
        /// </summary>
        public string? Description { get; set; }
    }
}
