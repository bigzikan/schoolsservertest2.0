﻿namespace schoolsservertest2._0.Data.Models.DTO
{
    public class School
    {
        /// <summary>
        /// Id школы
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Название школы
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Адрес школы
        /// </summary>
        public string Address { get; set; }
    }
}
