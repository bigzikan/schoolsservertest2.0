﻿using Microsoft.EntityFrameworkCore;
using schoolsservertest2._0.Data.Interfaces;
using schoolsservertest2._0.Data.Models.DTO;

namespace schoolsservertest2._0.Data.Repository
{
    public class ClassesRepository : IClass
    {
        private readonly SchoolsServerContext _appContext;

        public async Task<int> CreateClass(SchoolClass model)
        {
            await _appContext.Classes.AddAsync(model);
            _appContext.SaveChanges();
            return model.Id;
        }

        public async Task<List<SchoolClass>> GetAllClasses()
        {
            return await _appContext.Classes.ToListAsync();
        }

        public async Task<SchoolClass?> GetClassById(int id)
        {
            return await _appContext.Classes.FirstOrDefaultAsync(p => p.Id == id);
        }

        public async Task<List<Student>> GetStudentsByClassId(int classId)
        {
            return await _appContext.Students.Select(x => x).Where(p => p.ClassesId == classId).ToListAsync();
        }
    }
}
