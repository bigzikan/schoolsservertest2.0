﻿using Microsoft.EntityFrameworkCore;
using schoolsservertest2._0.Data.Interfaces;
using schoolsservertest2._0.Data.Models.DTO;

namespace schoolsservertest2._0.Data.Repository
{
    public class SchoolRepository : ISchool
    {
        private readonly SchoolsServerContext _appContext;

        public SchoolRepository(SchoolsServerContext appContext) 
        { 
            _appContext = appContext;
        }

        public async Task<List<School>> GetAllSchools()
        {
            return await _appContext.Schools.ToListAsync();
        }

        public async Task<School?> GetSchoolById(int id)
        {
            return await _appContext.Schools.FirstOrDefaultAsync(p => p.Id == id);
        }

        public async Task<int> CreateSchool(School model)
        {
            await _appContext.Schools.AddAsync(model);
            _appContext.SaveChanges();
            return model.Id;
        }

        public async Task<List<SchoolClass>> GetClassesBySchoolId(int schoolId)
        {   
            return await _appContext.Classes.Select(x => x).Where(p => p.SchoolId == schoolId).ToListAsync();
        }
    }
}
