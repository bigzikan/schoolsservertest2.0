﻿using Microsoft.EntityFrameworkCore;
using schoolsservertest2._0.Data.Interfaces;
using schoolsservertest2._0.Data.Models.DTO;

namespace schoolsservertest2._0.Data.Repository
{
    public class StudentsMigrationsRepository : IStudentsMigrations
    {
        private readonly SchoolsServerContext _appContext;

        public StudentsMigrationsRepository(SchoolsServerContext appContext)
        {
            _appContext = appContext;
        }

        private bool IsStudentInClassThisTime(StudentMigration model)
        {
            bool res = false;

            var studentsMigrations = _appContext.StudentsMigrations
                .Select(x => x)
                .Where(p => p.StudentId == model.StudentId & p.ClassId == model.ClassId)
                .ToList();

            foreach (var studentsMigration in studentsMigrations)
            {
                var currentClass = _appContext.Classes.FirstOrDefault(p => p.Id == model.ClassId);
                var currentSchool = new School();
                if (currentClass != null)
                {
                    currentSchool = _appContext.Schools.FirstOrDefault(p => p.Id == currentClass.SchoolId);
                }

                if (model.EnrollmentDate.HasValue
                    && model.EnrollmentDate >= model.EnrollmentDate
                    && model.EnrollmentDate <= model.DeductionDate)
                {
                    throw new Exception($"На момент выбранной даты зачисления ученик состоит в " +
                        $"{currentClass.ClassNumber}{currentClass.ClassLiteral} классе. Школа {currentSchool.Name}" +
                        $" Id миграции - {studentsMigration.Id}");
                }

                if (model.DeductionDate.HasValue
                    && model.DeductionDate >= model.EnrollmentDate
                    && model.DeductionDate <= model.DeductionDate)
                {
                    throw new Exception($"На момент выбранной даты отчисления ученик состоит в " +
                        $"{currentClass.ClassNumber}{currentClass.ClassLiteral} классе. Школа {currentSchool.Name}" +
                        $" Id миграции - {studentsMigration.Id}");
                }
            }

            return res;
        }

        public async Task<bool> AddStudentInСlass(StudentMigration model)
        {
            if (!model.EnrollmentDate.HasValue)
                throw new Exception("Должна быть указана дата зачисления в класс!");

            if (!IsStudentInClassThisTime(model))
            {
                await _appContext.StudentsMigrations.AddAsync(model);
                _appContext.SaveChanges();
            }

            return true;
        }

        public async Task<bool> RemoveStudentFromСlass(StudentMigration removeModel)
        {
            if (!removeModel.DeductionDate.HasValue)
                throw new Exception("Должна быть указана дата отчисления из класса!");

            var studentsMigrations = await _appContext.StudentsMigrations
                .FirstOrDefaultAsync(p => p.StudentId == removeModel.StudentId && p.ClassId == removeModel.ClassId && p.EnrollmentDate == removeModel.EnrollmentDate);

            if (studentsMigrations == null)
                throw new Exception("Выбранный школьник не зачислен в указанный класс!");


            if (!IsStudentInClassThisTime(removeModel))
            {
                _appContext.StudentsMigrations.Update(studentsMigrations);
                _appContext.SaveChanges();
            }

            return true;
        }

        public async Task<List<StudentMigration>> GetMigrationStudentHistory()
        {
            return await _appContext.StudentsMigrations.ToListAsync();
        }

        public async Task<List<StudentMigration>> GetMigrationStudentHistoryByStudentId(int studentId)
        {
            return await _appContext.StudentsMigrations
                .Select(x => x)
                .Where(p => p.StudentId == studentId)
                .ToListAsync();
        }
    }
}
