﻿using Microsoft.EntityFrameworkCore;
using schoolsservertest2._0.Data.Interfaces;
using schoolsservertest2._0.Data.Models.DTO;

namespace schoolsservertest2._0.Data.Repository
{
    public class StudentRepository : IStudent
    {
        private readonly SchoolsServerContext _appContext;

        public StudentRepository(SchoolsServerContext appContext)
        {
            _appContext = appContext;
        }

        public async Task<List<Student>> GetAllStudents()
        {
            return await _appContext.Students.ToListAsync();
        }

        public async Task<Student?> GetStudentById(int id)
        {
            return await _appContext.Students.FirstOrDefaultAsync(p => p.Id == id);
        }

        public async Task<int> CreateStudent(Student model)
        {
            await _appContext.Students.AddAsync(model);
            _appContext.SaveChanges();
            return model.Id;
        }
    }
}
