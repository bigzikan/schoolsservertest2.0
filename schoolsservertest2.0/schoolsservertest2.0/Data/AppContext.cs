﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using schoolsservertest2._0.Data.Models.DTO;

namespace schoolsservertest2._0.Data
{
    public class SchoolsServerContext : DbContext
    {
        public SchoolsServerContext(DbContextOptions<SchoolsServerContext> options) : base(options)
        { 
        }

        /// <summary>
        /// Таблица с инфо о школьниках
        /// </summary>
        public DbSet<Student> Students { get; set; }

        /// <summary>
        /// Таблица с инфо о школах
        /// </summary>
        public DbSet<School> Schools { get; set; }

        /// <summary>
        /// Таблица с инфо о классах
        /// </summary>
        public DbSet<SchoolClass> Classes { get; set; }

        /// <summary>
        /// Таблица с зачислениями/отчислениями
        /// </summary>
        public DbSet<StudentMigration> StudentsMigrations { get; set; }
    }
}
