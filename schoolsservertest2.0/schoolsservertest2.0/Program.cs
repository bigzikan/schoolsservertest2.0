using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using schoolsservertest2._0.Data;
using schoolsservertest2._0.Data.Interfaces;
using schoolsservertest2._0.Data.Repository;
using System.Runtime.CompilerServices;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddScoped<ISchool, SchoolRepository>();
builder.Services.AddScoped<IClass, ClassesRepository>();
builder.Services.AddScoped<IStudent, StudentRepository>();
builder.Services.AddScoped<IStudentsMigrations, StudentsMigrationsRepository>();

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(s =>
{
    s.SwaggerDoc("v1", new OpenApiInfo
    {
        Title = "SchoolsServer Backend",
        Version = "v1",
        Description = "API ������� SchoolsServer"
    });

    var xmlFile = $"{typeof(Program).Assembly.GetName().Name}.xml";
    var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
    s.IncludeXmlComments(xmlPath, includeControllerXmlComments: true);
});

AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);

builder.Services.AddHealthChecks();

builder.Services.AddEntityFrameworkNpgsql().AddDbContext<SchoolsServerContext>(options =>
{
    options.UseNpgsql(builder.Configuration.GetConnectionString("DefaultConnection") ?? 
        throw new InvalidOperationException("Connection String 'TaxiAggregatorServiceContext' not found."));
});

Microsoft.Extensions.Configuration.ConfigurationManager configuration = builder.Configuration;

var app = builder.Build();

app.UsePathBase(new PathString(configuration["AppName"]));
app.UseRouting();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI(options =>
    {
        string swaggerJsonBasePath = string.IsNullOrWhiteSpace(options.RoutePrefix) ? "." : "..";
        options.SwaggerEndpoint($"{swaggerJsonBasePath}/swagger/v1/swagger.json", "API");
        options.EnableFilter();
        options.DisplayRequestDuration();
    });
}

app.UseAuthorization();

app.MapControllers();

app.MapHealthChecks("/healthz");

app.Run();
